package com.mobiliha.unittesting.interval;

public class Interval {
    private final int mStart;
    private final int mEnd;

    public Interval(int start, int end) {
        if(start>=end){
            throw new IllegalArgumentException();
        }
        mStart = start;
        mEnd = end;
    }

    public int getStart() {
        return mStart;
    }

    public int getEnd() {
        return mEnd;
    }
}
