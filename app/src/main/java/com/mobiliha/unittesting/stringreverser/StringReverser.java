package com.mobiliha.unittesting.stringreverser;

import androidx.appcompat.app.AppCompatActivity;

public class StringReverser extends AppCompatActivity {

    public String reverse(String input) {
        StringBuilder reverse = new StringBuilder();
        if(input!=null) {
            for (int i = input.length() - 1; i >= 0; i--) {
                reverse.append(input.charAt(i));
            }
            return String.valueOf(reverse);
        }else return null;

    }
}