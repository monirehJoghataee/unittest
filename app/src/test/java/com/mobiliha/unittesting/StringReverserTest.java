package com.mobiliha.unittesting;

import androidx.annotation.Nullable;

import com.mobiliha.unittesting.stringreverser.StringReverser;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class StringReverserTest {
    StringReverser SUT;

    @Before
    public void setUp() throws Exception {
        SUT = new StringReverser();
    }

    @Nullable
    @Test
    public void reverse_emptyString_emptyStringReturned() {
        String res = SUT.reverse("");
        Assert.assertEquals(res, "");
    }

    @Test
    public void reverse_singleCharacter_sameStringReturned() {
        String res = SUT.reverse("a");
        Assert.assertEquals(res, "a");
    }

    @Test
    public void reverse_longString_reversedStringReturned() {
        String res = SUT.reverse("monireh");
        Assert.assertEquals(res, "herinom");
    }

    @Test
    @Nullable
    public void reverse_nullString_nullReturned() {
        String res=SUT.reverse(null);
        Assert.assertNull(res);
    }
}