package com.mobiliha.unittesting.interval;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class IntervalOverlapDetectorTest {
    IntervalOverlapDetector SUT;

    @Before
    public void setUp() throws Exception {
        SUT = new IntervalOverlapDetector();
    }

    @Test
    public void isOverlap_interval1BeforeInterval2_falseReturned() {
        Interval interval1 = new Interval(-1, 5);
        Interval interval2 = new Interval(8, 12);
        boolean res = SUT.isOverlap(interval1, interval2);
        Assert.assertFalse(res);
    }

    @Test
    public void isOverlap_interval2OverlapOnStart_trueReturned() {
        Interval interval1 = new Interval(-1, 5);
        Interval interval2 = new Interval(3, 12);
        boolean res = SUT.isOverlap(interval1, interval2);
        Assert.assertTrue(res);
    }

    @Test
    public void isOverlap_interval1BeforeAdjacentInterval2_falseReturned() {
        Interval interval1 = new Interval(-1, 5);
        Interval interval2 = new Interval(5, 8);
        boolean res = SUT.isOverlap(interval1, interval2);
        Assert.assertFalse(res);
    }

    @Test
    public void isOverlap_interval1AfterAdjacentInterval2_falseReturned() {
        Interval interval1 = new Interval(-1, 5);
        Interval interval2 = new Interval(-3, -1);
        boolean res = SUT.isOverlap(interval1, interval2);
        Assert.assertFalse(res);
    }


}